# encoding: utf-8
# frozen_string_literal: true

require 'awspec'

describe s3_bucket('omniopscorp-test-test-bucket-3') do
  it { should have_server_side_encryption(algorithm: "AES256") }
  it { should have_versioning_enabled }
end
