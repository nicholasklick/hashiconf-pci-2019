require 'awspec'

describe security_group('hashiconf2019-demo') do
  it { should exist }
end

describe security_group('hashiconf2019-demo') do
  its(:outbound) { should be_opened(443).protocol('tcp').for('0.0.0.0/0') }
end

describe security_group('hashiconf2019-demo') do
  its(:inbound) { should be_opened(22).protocol('tcp').for('172.31.0.0/16') }
end
