provider "aws" {
  region = "us-west-2"
}

module "s3" {
  source = "../../modules/s3"
  name   = "test-bucket-3"
  enable_versioning = true
  environment = "test"
  service     = "kitchen-demo"

  tags = {
    test        = "this-is-a-test-tag"
    Description = "test bucket"
  }
}
