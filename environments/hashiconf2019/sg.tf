resource "aws_security_group" "base" {
  name        = "hashiconf2019-demo"
  description = "Hashicorp 2019 demo"

  # Outbound HTTPS
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Allow inbound SSH
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["${aws_default_vpc.default.cidr_block}"]
  }

  tags = {
    Environment = var.environment
    Name = "Hashicorp 2019 demo SG"
  }
}
