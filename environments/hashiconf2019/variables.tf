variable "environment" {
  default     = "hashiconf2019"
  description = "Which environment does this belong to"
}
