resource "aws_kms_key" "hashiconf2019" {
  description             = "This is the hashiconf2019 KMS key"
  deletion_window_in_days = 14
}

resource "aws_kms_alias" "hashiconf2019" {
  name          = "alias/hashiconf2019"
  target_key_id = "${aws_kms_key.hashiconf2019.key_id}"
}
