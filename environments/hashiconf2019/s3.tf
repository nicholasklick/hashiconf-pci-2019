module "s3_bucket" {
  source = "../../modules/s3"
  name   = "hashiconf2019-demo"
  # enable_versioning = true
  environment = "test"
  service     = "kitchen-vpc"

  tags = {
    test        = "this-is-a-test-tag"
    service     = "demo"
    Description = "Hashicorp 2019 demo"
    Compliance  = "PCI"
  }
}
