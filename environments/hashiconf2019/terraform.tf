provider "aws" {
  region = "us-west-2"
}

terraform {
  required_version = "~> 0.12"

  backend "s3" {
    bucket         = "omniops-bootstrap-tfstate"
    key            = "hashiconf2019/terraform.tfstate"
    encrypt        = true
    region         = "us-west-2"
    dynamodb_table = "omniops-bootstrap-tfstate"
    kms_key_id     = "arn:aws:kms:us-west-2:374587566726:key/0e395695-caae-4749-bb35-c667094ebc35"
  }
}

data "aws_caller_identity" "current" {}
data "aws_region" "current" {}
data "aws_availability_zones" "all" {}
