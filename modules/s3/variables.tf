variable "name" {
  description = "Bucket name"
  type        = string
}

variable "service" {
  description = "Service name"
  type        = string
}

variable "environment" {
  type        = string
  description = "Which environment does this belong to"
}

variable "tags" {
  description = "A map of tags to add to all resources"
  default     = {}
}

variable "enable_versioning" {
  type    = bool
  default = false
}
