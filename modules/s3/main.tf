resource "aws_s3_bucket" "this" {
  bucket = "omniopscorp-${var.environment}-${var.name}"

  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm = "AES256"
      }
    }
  }

  versioning {
    enabled = var.enable_versioning
  }

  tags = merge(
    {
      Environment = var.environment,
      Billing     = "Hashiconf2019",
      Service     = var.service
    },
    var.tags
  )
}

# resource "aws_s3_bucket_public_access_block" "this" {
#   bucket = "${aws_s3_bucket.this.id}"
#
#   block_public_acls   = true
#   block_public_policy = true
# }
